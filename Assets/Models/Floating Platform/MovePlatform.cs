﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlatform : MonoBehaviour
{
    bool goingUp = true;
    [SerializeField] float maxHeight = 5;
    [SerializeField] float minHeight = 0;
    [SerializeField] float speed = 0.1f;
    float velocity = 0.1f;
    [SerializeField] float waitTime = 1f;
    Rigidbody myRigidbody;
    [SerializeField] Collider myCollider;
    [SerializeField] GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
        myCollider = GetComponent<Collider>();
    }

    IEnumerator delay()
    {
        velocity = 0;
        yield return new WaitForSeconds(waitTime);
        velocity = speed;

    }

    void OnCollisionEnter(Collision playerCollider)
    {
        goingUp = true;
    }

    // Update is called once per frame
    void Update()
    {
        float yPos = transform.position.y;

        

        if (goingUp == true)
        {
            if (yPos <= maxHeight)
            {
                myRigidbody.MovePosition(gameObject.transform.position + new Vector3(0f, velocity, 0f));
            }
            else
            {
                StartCoroutine(delay());
                goingUp = false;
            }
        }
        else
        {


            if (yPos >= minHeight)
            {
                myRigidbody.MovePosition(gameObject.transform.position + new Vector3(0f, -velocity, 0f));
            }
            else
            {
                StartCoroutine(delay());
                goingUp = true;
            }
        }
    }
}
