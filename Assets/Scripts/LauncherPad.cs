﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LauncherPad : MonoBehaviour
{
    [SerializeField] float force = 10;
    [SerializeField] [Range(-1.0f, 1.0f)] float directionX;
    [SerializeField] [Range(-1.0f, 1.0f)] float directionY;
    [SerializeField] [Range(-1.0f, 1.0f)] float directionZ;
    [SerializeField] float duration;
    [SerializeField] Animator animation;

    Vector3 direction =  Vector3.zero;

    void Start()
    {
        direction = new Vector3(directionX, directionY, directionZ);   
    }

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.CompareTag("Player"))
        {
            animation.Play("Launch");
            Rigidbody rb = c.transform.parent.parent.gameObject.GetComponent<Rigidbody>();
            rb.velocity = Vector3.zero;
            rb.AddForce(direction * force, ForceMode.Impulse);
            StartCoroutine(c.transform.parent.parent.gameObject.GetComponent<PlayerController>().DisableMovement(duration));
        }
    }
    
    void OnDrawGizmos()
    {
        direction = new Vector3(directionX, directionY, directionZ);
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, transform.position + direction * force / 10);
    }
}
