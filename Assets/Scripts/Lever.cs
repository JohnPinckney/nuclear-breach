﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : MonoBehaviour
{
    public bool isPressed;
    [SerializeField] Animator A;
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {

            if (!isPressed)
            {
                GameMaster.instance.EnableXIcon(true);
            }
            if (Input.GetButton("Fire3"))
            {
                isPressed = true;
                GameMaster.instance.EnableXIcon(false);
                A.SetBool("pulled", true);
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GameMaster.instance.EnableXIcon(false);
        }
    }
}
