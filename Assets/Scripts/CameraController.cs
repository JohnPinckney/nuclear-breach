﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] float verticalSpeed = 20;
    [SerializeField] float horizontalSpeed = 20;
    [SerializeField] float mouseSensitivity = 1;
    [SerializeField] Vector2 yClamp = new Vector2(0, 90);

    Vector3 offset;
    Vector3 eulerRotation;

    void Start()
    {
        offset = transform.parent.position - target.position;
    }

    void FixedUpdate()
    {
        transform.parent.position = target.position + offset;
        if (Input.GetAxis("RVertical") != 0 || Input.GetAxis("RHorizontal") != 0)
        {
            eulerRotation.y += Input.GetAxis("RHorizontal") * horizontalSpeed * Time.fixedDeltaTime;
            eulerRotation.x += Input.GetAxis("RVertical") * verticalSpeed * Time.fixedDeltaTime;
            eulerRotation.x = Mathf.Clamp(eulerRotation.x, yClamp.x, yClamp.y);
            Quaternion rotation = Quaternion.Euler(eulerRotation);
            transform.parent.localRotation = rotation;
        }
        else if (Input.GetAxis("Mouse Y") != 0 || Input.GetAxis("Mouse X") != 0)
        {
            eulerRotation.y += Input.GetAxis("Mouse X") * horizontalSpeed * mouseSensitivity * Time.fixedDeltaTime;
            eulerRotation.x += Input.GetAxis("Mouse Y") * verticalSpeed * mouseSensitivity *Time.fixedDeltaTime;
            eulerRotation.x = Mathf.Clamp(eulerRotation.x, yClamp.x, yClamp.y);
            Quaternion rotation = Quaternion.Euler(eulerRotation);
            transform.parent.localRotation = rotation;
        }
    }
}
