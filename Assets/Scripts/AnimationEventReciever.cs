﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventReciever : MonoBehaviour
{
    [SerializeField] PlayerController player;
    [SerializeField] AudioSource AS;
    [SerializeField] AudioClip[] suitStretch;
    [SerializeField] [Range(0, 1)] float StretchVolume;

    [SerializeField] AudioClip leftFoot;
    [SerializeField] AudioClip rightFoot;
    [SerializeField] [Range(0, 1)] float stepVolume;
    bool playLeftFoot;

    public void DisableThruster()
    {
        player.DisableThruster();
    }

    public void SuitStretch()
    {
        if (suitStretch.Length > 0)
        {
            int i = Random.Range(0, suitStretch.Length);
            AS.PlayOneShot(suitStretch[i], StretchVolume);
        }
    }

    public void Step()
    {
        if (playLeftFoot)
        {
            AS.PlayOneShot(leftFoot, stepVolume);
        }
        else
        {
            AS.PlayOneShot(rightFoot, stepVolume);
        }
        playLeftFoot = !playLeftFoot;
    }
}
