﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
public class GameMaster : MonoBehaviour
{

    [SerializeField] GameObject StartButton;
    [SerializeField] GameObject xButton;

    public static GameMaster instance;
    #region Pause Menu

    public bool isPaused;
    public LevelManager currentLevel;

    public GameObject Canvas;
    public void SetCurrentLevel(LevelManager l)
    {
        currentLevel = l;
    }
    public void Restart()
    {
        if (currentLevel != null)
        {
            currentLevel.ResetLevel();
            StartCoroutine(DelayUnpause());
        }
    }
    public void Continue()
    {
        StartCoroutine(DelayUnpause());
    }

    public void Quit()
    {
        Application.Quit();
    }

    IEnumerator DelayUnpause()
    {
        yield return new WaitForEndOfFrame();
        Time.timeScale = 1;
        Canvas.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);
        isPaused = false;
    }

    #endregion

    void Start()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        instance = this;
    }

    void Update()
    {
        if (Input.GetButtonDown("Pause"))
        {
            isPaused = !isPaused;
            EventSystem.current.SetSelectedGameObject(StartButton);
        }


        if(isPaused == true)
        {
            Time.timeScale = 0;

            Canvas.SetActive(true);
        }

        if(isPaused == false)
        {
            Time.timeScale = 1;
            Canvas.SetActive(false);
            EventSystem.current.SetSelectedGameObject(null);
        }
    }

    public void EnableXIcon(bool enabled)
    {
         xButton.SetActive(enabled);
    }
}
