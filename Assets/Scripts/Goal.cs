﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    [SerializeField] int nextLevel;
    [SerializeField] GameObject aObject;
    [SerializeField] Collider c;
    [SerializeField] float victoryAnimationTime;
    [SerializeField] float blackOutTime;


    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.CompareTag("Player"))
        {
            StartCoroutine(goToNextLevel());
        }
    }

    IEnumerator goToNextLevel()
    {
        c.enabled = false;
        aObject.SetActive(false);
        yield return new WaitForSeconds(victoryAnimationTime);
        SceneManager.LoadScene(nextLevel);
    }
}
