﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour
{
    [SerializeField] float rate;

    void Update()
    {
        Vector3 eulerAngles = transform.rotation.eulerAngles;
        eulerAngles.y -= rate * Time.deltaTime;
        transform.rotation = Quaternion.Euler(eulerAngles);
    }
}
