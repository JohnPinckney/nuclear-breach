﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public PlayerController player;

    [SerializeField] GameObject[] worldObjects;
    [SerializeField] Lever[] lervers;
    Vector3[] objectStartPoitions;

    void Start()
    {
        objectStartPoitions = new Vector3[worldObjects.Length];
        for (int i = 0; i < worldObjects.Length; i++)
        {
            objectStartPoitions[i] = worldObjects[i].transform.position;
        }
    }

    public void ResetLevel()
    {
        player.transform.position = transform.position;
        player.hasJump = false;
        player.trusterPack.SetActive(false);
        GameMaster.instance.EnableXIcon(false);
        for (int i = 0; i < lervers.Length; i++)
        {
            lervers[i].isPressed = false;
        }
        for (int i = 0; i < worldObjects.Length; i++)
        {
            worldObjects[i].transform.position = objectStartPoitions[i];
            worldObjects[i].SetActive(true);
        }
    }
}
