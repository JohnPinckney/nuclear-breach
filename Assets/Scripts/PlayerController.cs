﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public LevelManager currentLevel;

    Rigidbody RB;
    [SerializeField] float speed = 10;
    [SerializeField] float turnSpeed = 20;
    [SerializeField] Transform cam;
    bool movementEnabled = true;

    public bool hasJump = true;
    float jumpForce = 100;
    bool jumping;
    [SerializeField] LayerMask ground;
    [SerializeField] float jumpHeight = 3;
    [SerializeField] float jumpDistance = 2;

    [SerializeField] float maxGroundAngle = 120;
    Vector3 forward;
    float groundAngle;

    [SerializeField] float restartHoldDuration = 1.0f;
    float restartHoldTimer = 0.0f;

    [SerializeField] Animator playerAnimations;
    [SerializeField] ParticleSystem thruster;
    [SerializeField] GameObject m_ThrusterPack;

    [SerializeField] AudioSource AS;
    [SerializeField] AudioClip jump;
    [SerializeField] [Range(0, 1)] float jumpVolume;
    [SerializeField] AudioClip equipThruster;
    [SerializeField] [Range(0, 1)] float equipThrusterVolume;

    void Start()
    {
        RB = GetComponent<Rigidbody>();
        if (RB == null) Destroy(gameObject);
        jumpForce = (2 * jumpHeight * speed) / jumpDistance;
        Physics.gravity = new Vector3(0, (-2 * jumpHeight * speed * speed) / (jumpDistance * jumpDistance), 0);
    }

    void Update()
    {
        if (movementEnabled && !GameMaster.instance.isPaused)
        {
            Move();
            Jump();
        }
    }

    

    void Move()
    {
        
        Vector3 movement = Vector3.zero;
        Vector2 movementInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        float angle = Mathf.Atan2(movementInput.x, movementInput.y) * Mathf.Rad2Deg;
        angle += cam.rotation.eulerAngles.y;
        Vector3 right = Quaternion.Euler(0, angle, 0) * Vector3.right;
        Vector3 fwd = Quaternion.Euler(0, angle, 0) * Vector3.forward;

        RaycastHit hitInfo;
        bool rayHit = Physics.Raycast(transform.position + Vector3.up * 0.1f, -Vector3.up, out hitInfo, 1.0f, ground);
        if (rayHit)
        {
            forward = Vector3.Cross(right, hitInfo.normal);
        }
        else
        {
            forward = transform.forward;
        }
        if (rayHit)
        {
            groundAngle = Vector3.Angle(hitInfo.normal, fwd);
        }
        else
        {
            groundAngle = 90;
        }

        if (movementInput.x != 0 || movementInput.y != 0)
        {
            playerAnimations.SetBool("isWalking", true);
            Quaternion targetRotation = Quaternion.Euler(0, angle, 0);
            if (rayHit)
            {
                targetRotation.SetLookRotation(forward, hitInfo.normal);
                RaycastHit hit;
                if (Physics.Raycast(transform.position + Vector3.up * 0.1f, -transform.up, out hit, 0.22f, ground))
                {
                    if (hit.distance > 0.1f)
                    {
                        transform.position -= transform.up * (hit.distance - 0.1f);
                    }
                }
            }
            targetRotation = Quaternion.Euler(targetRotation.eulerAngles.x, angle, targetRotation.eulerAngles.z);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, turnSpeed * Time.deltaTime);
            if (groundAngle < maxGroundAngle)
            {
                movement = transform.forward * speed * movementInput.magnitude;
            }
        }
        else
        {
            playerAnimations.SetBool("isWalking", false);
        }
        if (!Grounded() || jumping)
        {
            RB.velocity = new Vector3(movement.x, RB.velocity.y, movement.z);
        }
        else
        {
            RB.velocity = movement;
        }
    }

    void Jump()
    {
        if (hasJump)
        {
            if (Input.GetButtonDown("Jump"))
            {
                playerAnimations.SetBool("isJumping", true);
                thruster.Play();
                AS.PlayOneShot(jump, jumpVolume);
                jumping = true;
                RB.velocity = new Vector3(RB.velocity.x, jumpForce, RB.velocity.z);
                hasJump = false;
            }
        }
    }

    public IEnumerator DisableMovement(float t)
    {
        movementEnabled = false;
        yield return new WaitForSeconds(t);
        movementEnabled = true;
    }

    bool Grounded()
    {
        return Physics.BoxCast(transform.position + transform.up * 0.2f, new Vector3(1, 0.1f, 1), -transform.up, Quaternion.Euler(0, 0, 0), 0.3f, ground);
    }

    void OnCollisionEnter(Collision c)
    {
        if (ground == (ground | 1 << c.gameObject.layer))
        {
            float angle = Vector3.Angle(c.contacts[0].normal, transform.forward);
            if (angle < maxGroundAngle)
            {
                jumping = false;
                playerAnimations.SetBool("isJumping", false);
                movementEnabled = true;
                StopAllCoroutines();
            }
        }

        if (c.gameObject.CompareTag("Enemy"))
        {
            if (transform.position.y > c.transform.position.y)
            {
                RB.velocity = new Vector3(RB.velocity.x, jumpForce, RB.velocity.z);
            }
        }
    }

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.CompareTag("Fuel"))
        {
            if (!hasJump)
            {
                AS.PlayOneShot(equipThruster, equipThrusterVolume);
                hasJump = true;
                c.gameObject.SetActive(false);
                m_ThrusterPack.SetActive(true);
            }
        }

        if (c.gameObject.CompareTag("checkPoint"))
        {
            currentLevel = c.gameObject.GetComponent<LevelManager>();
            currentLevel.player = this;
        }
    }

    public void DisableThruster()
    {
        m_ThrusterPack.SetActive(false);
    }

    public GameObject trusterPack { get { return m_ThrusterPack; } }
}
