﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeScript : MonoBehaviour
{
    public Lever lever;

    public Vector3 endPoint;

    public float Speed = 3f;


    public void Update()
    {
        if (lever.isPressed == true)
        {
            CallBridge();
        }
    }
    public void CallBridge()
    {

        float step = Speed * Time.deltaTime;
        gameObject.transform.position = Vector3.MoveTowards(transform.position, endPoint, step);



    }
}
 

