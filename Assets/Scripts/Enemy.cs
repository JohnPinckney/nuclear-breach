﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Transform StartPoint;
    public Transform EndPoint;

    public int Index;

    public bool IsMovingEndPoint;

    public bool IsMovingStartPoint;

    public float speed = 1.0f;
    void Start()
    {
        transform.position = StartPoint.position;
    }
    void Update()
    {
        float step = speed * Time.deltaTime; // calculate distance to move




        if (IsMovingStartPoint == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, StartPoint.position, step);

        }
        if (IsMovingEndPoint == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, EndPoint.position, step);

        }

        if (transform.position == EndPoint.transform.position)
        {
            IsMovingStartPoint = true;
            IsMovingEndPoint = false;
        }

        if (transform.position == StartPoint.transform.position)
        {
            IsMovingEndPoint = true;
            IsMovingStartPoint = false;
        }
    }
}
