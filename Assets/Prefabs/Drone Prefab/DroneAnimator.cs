﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneAnimator : MonoBehaviour
{
    [SerializeField] Animator A;

    void OnCollisionEnter(Collision c)
    {
        if (c.collider.gameObject.CompareTag("Player"))
        {
            A.SetTrigger("Hit");
        }
    }
}
